package com.cuesocialcalendar.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.cuesocialcalendar.R
import com.cuesocialcalendar.data.model.EventModel
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.measure_item.*

class EventAdapter(
    private val fragment: Fragment,
    diffCallback: DiffUtil.ItemCallback<EventModel>
) : PagingDataAdapter<EventModel, EventAdapter.ViewHolder>(diffCallback) {
    private val layoutInflater = LayoutInflater.from(fragment.requireContext())
    private val listener = fragment as OnBindViewHolderListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = layoutInflater.inflate(R.layout.measure_item, parent, false)
        return ViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bindData(getItem(position)!!)

    class ViewHolder(
        itemView: View,
        private val listener: OnBindViewHolderListener
    ) : RecyclerView.ViewHolder(itemView), LayoutContainer {
        override val containerView: View = itemView

        fun bindData(item: EventModel) {
            listener.onBind(item)
            val subTitle = "${item.dayOfMonth}.${item.month}.${item.year}"
            val allDay = "All Day"
            val dayOfWeek = "MON"
            tv_title.text = "Independent Day"
            tv_day_number.text = item.dayOfMonth.toString()
            tv_sub_title.text = subTitle
            tv_all_day.text = allDay
            tv_day_name.text = dayOfWeek
        }
    }

    interface OnBindViewHolderListener {
        fun onBind(event: EventModel)
    }
}