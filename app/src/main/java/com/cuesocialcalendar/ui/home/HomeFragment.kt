package com.cuesocialcalendar.ui.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.cuesocialcalendar.R
import com.cuesocialcalendar.`object`.EventComparator
import com.cuesocialcalendar.data.model.EventModel
import com.cuesocialcalendar.ui.home.adapter.EventAdapter
import com.cuesocialcalendar.ui.home.adapter.EventLoadStateAdapter
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class HomeFragment : Fragment(R.layout.fragment_home), EventAdapter.OnBindViewHolderListener {
    private val viewModel by viewModels<HomeViewModel>()
    private var calendarShow = true
    private var lastEvent: EventModel? = null
    private var penultimateEvent: EventModel? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        calendarView.visibility = View.GONE
        val eventAdapter = EventAdapter(this@HomeFragment, EventComparator)
        rv_list.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter =
                eventAdapter.withLoadStateFooter(
                    footer = EventLoadStateAdapter { eventAdapter.retry() }
                )
        }

        lifecycleScope.launch {
            viewModel.events.collectLatest { pagingData ->
                eventAdapter.submitData(pagingData)
            }
        }

        tv_month.setOnClickListener {
            hideShowCalendar()
        }

        iv_arrow.setOnClickListener {
            hideShowCalendar()
        }

        calendarView.setOnDateChangeListener { calendarView, year, month, day ->
            Timber.d("$day ${month + 1} $year")
            val data = viewModel.getDatabase()
            for (events in data) {
                for (event in events.events) {
                    if (event.dayOfMonth == day.toLong() && event.month == month.toLong() &&
                        event.year == year.toLong()
                    ) {
                    }
                }
            }
        }
    }

    private fun hideShowCalendar() {
        if (calendarShow) {
            calendarView.visibility = View.VISIBLE
            iv_arrow.setImageResource(R.drawable.ic_baseline_keyboard_arrow_up_24)
            calendarShow = false
        } else {
            calendarView.visibility = View.GONE
            iv_arrow.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24)
            calendarShow = true
        }
    }

    @SuppressLint("SimpleDateFormat")
    override fun onBind(event: EventModel) {
        penultimateEvent = lastEvent
        lastEvent = event
        val calendar = Calendar.getInstance()
        val sdf = SimpleDateFormat("dd.MM.yy")
        try {
            val data: EventModel = if (penultimateEvent != null) {
                penultimateEvent!!
            } else {
                lastEvent!!
            }
            val mDate: Date = sdf.parse("${data.dayOfMonth}.${data.month}.${data.year}")!!
            val timeInMillis: Long = mDate.time
            calendar.timeInMillis = timeInMillis
            calendarView.setDate(calendar.timeInMillis, true, true)
            tv_month.text = data.monthName
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }
}