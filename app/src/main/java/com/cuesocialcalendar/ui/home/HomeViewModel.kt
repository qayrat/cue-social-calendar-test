package com.cuesocialcalendar.ui.home

import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.cuesocialcalendar.api.EventPagingSource
import com.cuesocialcalendar.data.model.EventModel
import com.cuesocialcalendar.data.model.PagerList
import kotlinx.coroutines.flow.Flow

class HomeViewModel : ViewModel() {
    val events: Flow<PagingData<EventModel>> = Pager(
        config = PagingConfig(pageSize = 8)
    ) {
        EventPagingSource(getDatabase())
    }.flow

    fun getDatabase(): ArrayList<PagerList> {
        val list = ArrayList<PagerList>()
        val eventList = ArrayList<EventModel>()
        for (i in year.indices) {
            eventList.add(
                EventModel(
                    id = i + 1,
                    monthName = monthName[i],
                    month = month[i],
                    year = year[i],
                    dayOfMonth = dayOfMonth[i]
                )
            )
        }
        list.add(PagerList(1, eventList.subList(0, 8).toList()))
        list.add(PagerList(1, eventList.subList(8, 16).toList()))
        list.add(PagerList(1, eventList.subList(16, 24).toList()))
        list.add(PagerList(1, eventList.subList(24, 32).toList()))
        return list
    }

    companion object {
        private val monthName = arrayOf(
            "January", "February", "April", "September", "December", "July", "May", "October",
            "May", "August", "September", "December", "November", "March", "July", "June",
            "April", "March", "January", "September", "December", "April", "August", "September",
            "February", "August", "March", "July", "September", "June", "April", "February"
        )
        private val month = longArrayOf(
            1, 2, 4, 9, 12, 7, 5, 10,
            5, 8, 9, 12, 11, 3, 7, 6,
            4, 3, 1, 9, 12, 4, 8, 9,
            2, 8, 3, 7, 9, 6, 4, 2
        )
        private val year =
            longArrayOf(
                2020, 2018, 2015, 2022, 2024, 2023, 2025, 2012,
                2021, 2016, 2014, 2023, 2024, 2026, 2024, 2021,
                2022, 2028, 2010, 2021, 2022, 2021, 2025, 2012,
                2023, 2017, 2015, 2011, 2024, 2022, 2022, 2019
            )
        private val dayOfMonth = longArrayOf(
            1, 16, 5, 1, 31, 28, 10, 7,
            4, 21, 24, 12, 11, 30, 8, 2,
            5, 28, 14, 11, 22, 6, 15, 17,
            17, 27, 12, 13, 25, 12, 23, 29,
        )
    }
}