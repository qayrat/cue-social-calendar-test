package com.cuesocialcalendar.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cuesocialcalendar.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.load_state_view.*

class EventLoadStateAdapter(
    private val retry: () -> Unit
) : LoadStateAdapter<EventLoadStateAdapter.LoadStateViewHolder>() {
    override fun onBindViewHolder(
        holder: LoadStateViewHolder,
        loadState: LoadState
    ) {
        holder.bindData(loadState)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): LoadStateViewHolder {
        return LoadStateViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.load_state_view, parent, false)
        )
    }

    inner class LoadStateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {
        override val containerView: View
            get() = itemView

        fun bindData(loadState: LoadState) {
            load_state_retry.isVisible = loadState !is LoadState.Loading
            load_state_errorMessage.isVisible = loadState !is LoadState.Loading
            load_state_progress.isVisible = loadState !is LoadState.Loading

            if (loadState is LoadState.Error) {
                load_state_errorMessage.text = loadState.error.localizedMessage
            }

            load_state_retry.setOnClickListener {
                retry.invoke()
            }
        }
    }
}