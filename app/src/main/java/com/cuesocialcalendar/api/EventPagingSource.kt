package com.cuesocialcalendar.api

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.cuesocialcalendar.data.model.EventModel
import com.cuesocialcalendar.data.model.PagerList
import timber.log.Timber

class EventPagingSource(private val data: ArrayList<PagerList>) :
    PagingSource<Int, EventModel>() {

    override fun getRefreshKey(state: PagingState<Int, EventModel>): Int? {
        val anchorPosition = state.anchorPosition ?: return null
        val page = state.closestPageToPosition(anchorPosition) ?: return null
        return page.prevKey?.plus(1) ?: page.nextKey?.minus(1)
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, EventModel> {
        val page = params.key ?: FIRST_PAGE_INDEX
        val eventList: List<EventModel> = data[page - 1].events
        Timber.d("list size : ${data[page - 1].events.size}")
        val nextKey = if (page == 4) null else page + 1
        val prevKey = if (page == 1) null else page - 1
        Timber.d("page : $page")
        Timber.d("nextKey : $nextKey")
        Timber.d("prevKey : $prevKey")
        return LoadResult.Page(eventList, prevKey, nextKey)
//        val response = apiService.getEvents(page, pageSize)
//        return if (response.isSuccessful) {
//            val data = checkNotNull(response.body()).data
//            val eventList = App.database.eventDao().getAll()[page].events
//            val nextKey = if (eventList.size < pageSize) null else page + 1
//            val prevKey = if (page == 1) null else page - 1
//            LoadResult.Page(eventList, prevKey, nextKey)
//        } else {
//            LoadResult.Error(HttpException(response))
//        }
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 1
    }
}