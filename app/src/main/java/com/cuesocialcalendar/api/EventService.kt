package com.cuesocialcalendar.api

import com.cuesocialcalendar.data.model.EventCalendar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface EventService {

    @GET("v1/passenger")
    suspend fun getEvents(
        @Query("page") query: Int,
        @Query("size") page: Int,
    ): Response<EventCalendar>

    companion object {
        private const val BASE_URL = "https://api.instantwebtools.net/"

        fun create(): EventService {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(EventService::class.java)
        }
    }
}