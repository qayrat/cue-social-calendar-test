package com.cuesocialcalendar.data.model

import com.google.gson.annotations.SerializedName

class EventData(
    @SerializedName("_id") val id: String,
    val name: String,
    val trips: Int,
    val airline: ArrayList<Airline>,
    val __v: Int
)