package com.cuesocialcalendar.data.model

class EventCalendar(
    val totalPassengers: Int,
    val totalPages: Int,
    val data: ArrayList<EventData>
)