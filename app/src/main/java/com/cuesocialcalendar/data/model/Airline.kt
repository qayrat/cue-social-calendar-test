package com.cuesocialcalendar.data.model

import com.google.gson.annotations.SerializedName

class Airline(
    val id: Int,
    val name: String,
    val country: String,
    val logo: String,
    val slogan: String,
    @SerializedName("head_quaters") val headQuarters: String,
    val website: String,
    val established: String
)