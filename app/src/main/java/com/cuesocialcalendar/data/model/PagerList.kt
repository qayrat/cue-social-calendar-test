package com.cuesocialcalendar.data.model

data class PagerList(
    val id: Int,
    val events: List<EventModel>
)