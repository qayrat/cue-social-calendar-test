package com.cuesocialcalendar.data.model

class EventModel(
    val id: Int,
    val monthName: String,
    val month: Long,
    val year: Long,
    val dayOfMonth: Long
)