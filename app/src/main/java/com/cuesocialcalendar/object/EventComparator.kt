package com.cuesocialcalendar.`object`

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil
import com.cuesocialcalendar.data.model.EventModel

object EventComparator :
    DiffUtil.ItemCallback<EventModel>() {
    override fun areItemsTheSame(oldEvent: EventModel, newEvent: EventModel) =
        oldEvent.id == newEvent.id

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldEvent: EventModel, newEvent: EventModel): Boolean {
        return oldEvent == newEvent
    }
}